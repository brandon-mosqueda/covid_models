rm(list=ls())

source("models.R")

Covid_data <- read.csv("Covid_cases.csv")

models_names <- c("M1_logistic", "M2_exponential", "M3_gompertz",
                  "M3.1_gompertz_poisson", "M4_bertalanffy")

N <- nrow(Covid_data)
last_recorded_day <- Covid_data$Date[N]
predict_until_date <- "2020-06-15"
number_days_to_predict <- as.numeric(as.Date(predict_until_date) -
                                     as.Date(last_recorded_day))

Cases <- data.frame(counts=Covid_data$Accumulated_cases,
                    date=Covid_data$Date,
                    day=1:N)

first_death_day_index <- which(Covid_data$Accumulated_deaths != 0)[1]
Deaths <- data.frame(counts=Covid_data[first_death_day_index:N, ]$Accumulated_deaths,
                     date=Covid_data[first_death_day_index:N, ]$Date,
                     day=1:(N - first_death_day_index + 1))

n_weeks_to_remove <- 3

for (i in 0:n_weeks_to_remove) {
    days_to_remove <- i * 7
    cat("Sin ", days_to_remove, " días:\n")

    if (days_to_remove > 0) {
        Cases <- Cases[-(1:days_to_remove), ]
        Cases$day <- 1:nrow(Cases)
        if (i <= 2) {
            Deaths <- Deaths[-(1:days_to_remove), ]
            Deaths$day <- 1:nrow(Deaths)
        }
    }

    accumulated_cases_predictions <- matrix(
        , nrow=number_days_to_predict + nrow(Cases), ncol=0)
    accumulated_deaths_predictions <- matrix(
        , nrow=number_days_to_predict + nrow(Deaths), ncol=0)
    AICs_cases <- 1:length(models_names)
    AICs_deaths <- 1:length(models_names)

    for (j in 1:length(models_names)) {
        current_model <- models_names[j]
        cat("   ", current_model, "\n")

        params <- list(y=Cases$counts,
                       t=Cases$day,
                       lag=number_days_to_predict)
        model_predictions <- do.call(current_model, params)
        accumulated_cases_predictions <- cbind(accumulated_cases_predictions,
                                               model_predictions$yp)
        AICs_cases[j] <- model_predictions$AIC

        plot_accumulated(Cases$counts, model_predictions$yp,
                         model_name=current_model,
                         n_head_removed_days=days_to_remove,
                         predicting="confirmed_cases",
                         first_day=Cases[1, "date"],
                         last_day=predict_until_date)

        plot_daily(Cases$counts, model_predictions$yp,
                   model_name=current_model,
                   n_head_removed_days=days_to_remove,
                   predicting="confirmed_cases",
                   first_day=Cases[1, "date"],
                   last_day=predict_until_date)


        # Deats
        if (i <= 2) {
            params <- list(y=Deaths$counts,
                           t=Deaths$day,
                           lag=number_days_to_predict)
            model_deaths_predictions <- do.call(current_model, params)
            accumulated_deaths_predictions <- cbind(accumulated_deaths_predictions,
                                                    model_deaths_predictions$yp)
            AICs_deaths[j] <- model_deaths_predictions$AIC

            plot_accumulated(Deaths$counts, model_deaths_predictions$yp,
                             model_name=current_model,
                             n_head_removed_days=days_to_remove,
                             predicting="deaths",
                             first_day=Deaths[1, "date"],
                             last_day=predict_until_date)

            plot_daily(Deaths$counts, model_deaths_predictions$yp,
                       model_name=current_model,
                       n_head_removed_days=days_to_remove,
                       predicting="deaths",
                       first_day=Deaths[1, "date"],
                       last_day=predict_until_date)
        }
    }

    colnames(accumulated_cases_predictions) <- models_names

    csv_accumulated(Cases$counts, accumulated_cases_predictions,
                    first_day=Cases[1, "date"], last_day=predict_until_date,
                    AICs=AICs_cases, predicting="confirmed_cases",
                    n_head_removed_days=days_to_remove)

    csv_daily(Cases$counts, accumulated_cases_predictions,
              first_day=Cases[1, "date"], last_day=predict_until_date,
              AICs=AICs_cases, predicting="confirmed_cases",
              n_head_removed_days=days_to_remove)

    if (i <= 2) {
        colnames(accumulated_deaths_predictions) <- models_names

        csv_accumulated(Deaths$counts, accumulated_deaths_predictions,
                        first_day=Deaths[1, "date"], last_day=predict_until_date,
                        AICs=AICs_deaths, predicting="deaths",
                        n_head_removed_days=days_to_remove)

        csv_daily(Deaths$counts, accumulated_deaths_predictions,
                  first_day=Deaths[1, "date"], last_day=predict_until_date,
                  AICs=AICs_deaths, predicting="deaths",
                  n_head_removed_days=days_to_remove)
    }
}